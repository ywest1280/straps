//index.html에 사용될 js 스크립트
import {
  putProduct,
  deleteProduct,
  postProduct,
  getProducts,
} from './modules/product.mjs';
window.localStorage.setItem("cartItems",JSON.stringify([]));
//서버 주소
// const productsUrl = "http://34.64.218.104:3000/products";

////////////////////
//getProducts DEMO
// const products = getProducts(productsUrl);
// console.log(products);
////////////////////

////////////////////////////////////////////////
// postProduct DEMO
// let dataForPostDemo =   {
//     "name": "PostDemo",
//     "kind": 1,
//     "price": 2522,
//     "content": "testcontent13",
//     "image": "http://dummyimage.com/183x100.png/dddddd/000000",
//     "regdate": "6/9/2022",
//     "prod_sell": 3
//   };
// const callPostProduct = postProduct(productsUrl,dataForPostDemo);
// console.log(callPostProduct);
////////////////////////////

////////////////////////////////////////////////
//putProduct DEMO

// let dataForPutDemo = {
//   "name": "PutDemo",
//   "kind": 1,
//   "price": 962,
//   "content": "test1010",
//   "image": "http://naver.com/183x100.png/5fa2dd/ffffff",
//   "regdate": "9/23/2025",
//   "prod_count": 501,
//   "prod_sell": 91,
//   "prod_seq": 10
// };

// let putUrlParams = 10;

// const callPutProduct = putProduct(productsUrl, putUrlParams, dataForPutDemo);
// console.log(callPutProduct);
////////////////////////////

////////////////////////////////////////////////
//deleteProduct DEMO

// let deleteUrlParams = 2;

// const callDeleteProduct = deleteProduct(productsUrl, deleteUrlParams);
// console.log(callDeleteProduct);
////////////////////////////

////////////////////
//getProducts DEMO
//const callGetUser = getUser(userUrl, userDataForChangeDemo['email']);
//console.log(callGetUser);
////////////////////

////////////////////////////////////////////////
//deleteProduct DEMO

//let deleteUserEmail = userDataForChangeDemo['email'];

//const callDeleteUser = deleteUser(userUrl, deleteUserEmail);
//console.log(callDeleteUser);
////////////////////////////

/*
//로컬스토리지 어쩌구저쩌구 중
const addToCartBtns = document.querySelectorAll('.add-to-cart-btn');
addToCartBtns.forEach((btn) => {
  btn.addEventListener('click', addItemFunction);
});

//addToCartBtns 변수에 할당된 요소..? 여기서부터 막혔죠ㅠㅠㅠㅠㅠㅠㅠㅠㅠ

class CartItem {
  constructor(name, img, price) {
    this.name = name;
    this.img = img;
    this.price = price;
    this.quantity = 1;
  }
}

class LocalCart {
  static key = 'cartItems';

  static getLocalCartItems() {
    let cartMap = new Map();
    const cart = localStorage.getItem(LocalCart.key);
    if (cart === null || cart.length === 0) return cartMap;
    return new Map(Object.entries(JSON.parse(cart)));
  }
  static addItemToLocalCart(id, item) {
    let cart = LocalCart.getLocalCartItems();
    if (cart.has(id)) {
      let mapItem = cart.get(id);
      mapItem.quantity += 1;
      cart.set(id, mapItem);
    } else cart.set(id, item);
    localStorage.setItem(
      LocalCart.key,
      JSON.stringify(Object.fromEntries(cart))
    );
    updateCartUI();
  }
  static removeItemFromCart(id) {
    let cart = LocalCart.getLocalCartItems();
    if (cart.has(id)) {
      let mapItem = cart.get(id);
      if (mapItem.quantity > 1) {
        mapItem.quantity -= 1;
        cart.set(id, mapItem);
      } else cart.delete(id);
    }
    if (cart.length === 0) localStorage.clear();
    else
      localStorage.setItem(
        LocalCart.key,
        JSON.stringify(Object.fromEntries(cart))
      );
    updateCartUI();
  }
}

//장바구니 아이콘에 상품수량 표시
const iconShoppingP = document.querySelector('.add-to-cart-btn');
let no = 0;
JSON.parse(localStorage.getItem('item')).map((data) => {
  no = no + data.no;
});
iconShoppingP.innerHTML = no;
*/

const cartItemsObj = JSON.parse(window.localStorage.getItem("cartItems"));
let sum = 0;
try{
  cartItemsObj.array.forEach(element => {
    sum = Number(element.price) + sum;
  });
  document.querySelector("#cartSum").innerHTML = sum.toString() +" "+"원";
}
catch(error){
  console.log(error);
}


//장바구니 로컬스토리지 어쩌구저쩌구 중..
const addToCartBtns = document.querySelectorAll('.add-to-cart-btn');
console.log(addToCartBtns);
addToCartBtns.forEach((btn) => {
  btn.addEventListener('click', addItemFunction);
});

function addItemFunction(e) {
  const id = e.target.closest('.card-item').getAttribute('data-id');
  const name = e.target.closest('.details').querySelector('h3').innerText;
  const img = e.target.closest('.card-item').querySelector('img').src;
  const price = e.target.closest('.details').querySelector('.price').innerText;
  const cartItem = new CartItem(name, img, price);
  LocalCart.addItemToLocalCart(parseInt(id), cartItem);
}

class CartItem {
  constructor(name, img, price) {
    this.name = name;
    this.img = img;
    this.price = price;
    this.quantity = 1;
  }
}

class LocalCart {
  static key = 'cartItems';

  static getLocalCartItems() {
    let cartMap = new Map();
    const cart = localStorage.getItem(LocalCart.key);
    if (cart === null || cart.length === 0) return cartMap;
    return new Map(Object.entries(JSON.parse(cart)));
  }
  static addItemToLocalCart(id, item) {
    let cart = LocalCart.getLocalCartItems();
    if (cart.has(id)) {
      let mapItem = cart.get(id);
      mapItem.quantity += 1;
      cart.set(id, mapItem);
    } else cart.set(id, item);
    localStorage.setItem(
      LocalCart.key,
      JSON.stringify(Object.fromEntries(cart))
    );
    updateCartUI();
  }
  /*static addItemToLocalCart(id, item) {
    let cart = LocalCart.getLocalCartItems();
    if (cart.has(parseInt(id))) {
      let mapItem = cart.get(parseInt(id));
      mapItem.quantity += 1;
      cart.set(parseInt(id), mapItem);
    } else cart.set(parseInt(id), item);
    localStorage.setItem(
      LocalCart.key,
      JSON.stringify(Array.from(cart.entries()))
    );
    updateCartUI();
  }
*/
  static removeItemFromCart(id) {
    let cart = LocalCart.getLocalCartItems();
    if (cart.has(id)) {
      let mapItem = cart.get(id);
      if (mapItem.quantity > 1) {
        mapItem.quantity -= 1;
        cart.set(id, mapItem);
      } else cart.delete(id);
    }
    if (cart.length === 0) localStorage.clear();
    else
      localStorage.setItem(
        LocalCart.key,
        JSON.stringify(Object.fromEntries(cart))
      );
    updateCartUI();
  }
}

window.addEventListener('load', () => {
  const loginBtn = document.querySelector("#loginButton");
  const token = localStorage.getItem("JWT");
  if(token){
    loginBtn.querySelectorAll("path").forEach(path => path.setAttribute("fill", "red"));    
  }
  
});